<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/{lang?}', 'HomeController@index')
                ->where(['lang' => '[a-zA-Z]{2}']);

// Route::resource('product', 'ProductController'); 

// Route::get('{lang}/products/{slug}', 'ProductController@ficheProd')
//     // ->where('lang', '[a-z]2')
//     ->where('slug' ,'^[a-z0-9]+(?:-[a-z0-9]+)*$');


Route::group([
    'prefix' => '{lang}', 
    'where' => ['lang' => '[a-zA-Z]{2}']
    ], function() {
        Route::get('products/{slug}', 'ProductController@ficheProd')
        ->where(['slug' => '^[a-z0-9]+(?:-[a-z0-9]+)*$']); 
}); 

Route::get('loginPage', 'HomeController@login')->name('login.page.get');
Route::get('logout', array('uses' => 'HomeController@doLogout'));

Route::get('homePage', 'ProductController@homePage')->name('products.homepage.get');
Route::get('bucketsPage', 'BucketsController@showPublishedBuckets')->name('featured.buckets.get');
