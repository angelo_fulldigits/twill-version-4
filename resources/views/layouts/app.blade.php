<!DOCTYPE html>
<html dir="ltr" lang="en-US">
    <head>
        <meta charset="utf-8" />
        <title>Twillv4 - @yield('title')</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"> 
        
        <style>
        @import url(https://fonts.googleapis.com/css?family=Raleway:300,400,600);
             body{
                margin: 0;
                font-size: .9rem;
                font-weight: 400;
                line-height: 1.6;
                color: #212529;
                text-align: left;
                background-color: #f5f8fa;
                
            } 
    
            .navbar-laravel
            {
                box-shadow: 0 2px 4px rgba(0,0,0,.04);
            }
            .top-right {
                position: absolute;
                width:auto;
                height: :auto;
                right: 10px;
                top: 18px;
            }
            .top-left {
                position: absolute;
                width:auto;
                height: :auto;
                left: 10px;
                top: 18px;
            }
            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }
            .nav-logout{
                color:red;
            }
            .m-toph1{
                margin-top: 10%;
            }
            .m-top{
                margin-top: 0.1%;
            }
    
            .card{
                background: #f2faf9;
                -webkit-box-shadow: 9px 10px 19px -6px rgba(0,0,0,0.4);
                -moz-box-shadow: 9px 10px 19px -6px rgba(0,0,0,0.4);
                box-shadow: 9px 10px 19px -6px rgba(0,0,0,0.4);
            }
    
            .zoom { 
                transition: transform .3s;  /* Animation */ 
            }
    
            .zoom:hover {
                transform: scale(1.02); /* (150% zoom - Note: if the zoom is too large, it will go outside of the viewport) */
            }
            .img-zoom { 
                transition: transform .8s;  /* Animation */ 
            }
    
            .img-zoom:hover {
                transform: scale(1.3); /* (150% zoom - Note: if the zoom is too large, it will go outside of the viewport) */
            } 
            .title{
                font-size: 50px;
            } 
            .img-mtop {
                margin-top: 15%;
            } 
            .img-mtop2 {
                margin-top: 10%;
            }  

            .group { 
                display: table;
                width: 50%;
                font-size: 2.5em;
                margin: 0 auto;
            } 
            .item {
                display: table-cell;
            }
            .text { 
                white-space: nowrap;
                width: 1%;
                padding: 0 10px;
            }
            .line {
                border-bottom: 1px solid #000;
                position: relative;
                top: -.5em;
            }
        </style>
    </head>
    <body>
        @section('sidebar')
            This is the master sidebar.
        @show

        <div class="container">
            @yield('content')
        </div>
    </body>
</html>