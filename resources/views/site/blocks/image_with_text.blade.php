<div class="image_with_text">
    <h3>Image</h3>
    <div>
        <img src="{{ $block->image('cover', 'default')  }}" width="25%" height="25%" alt="">
    </div>
    <h3>Subtitle</h3>
    <div>
        {{ $block->translatedinput('image_subtitle') }} 
    </div>
</div>