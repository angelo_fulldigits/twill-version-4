<style>
    .rounded{
        margin:1% 1.5%;
        border-radius: .25rem !important;
    }
    .img-thumbnail {
        padding: .25rem;
        background-color: #fff;
        border: 0px solid #dee2e6;
        border-radius: .25rem;
        max-width: 100%;
        height: auto
    }
    .text-center {
        text-align: center !important
    }
     
    .container-p {
        width: 70%; 
        margin: auto auto; 
    }
    .container {
        width: 100%;
        padding-right: 15px;
        padding-left: 15px;
        margin-right: auto;
        margin-left: auto
    }
    .float-left {
        float: left !important
    }

    .float-right {
        float: right !important
    }

</style>
<div class='hero container'>
    <div class="text-center">
        @if( $item->hasImage('hero_image'))
            <img src="{{ $item->image('hero_image', 'default') }}"  class="rounded img-thumbnail">
        @endif
    </div>
    <div class="text-center">
        <h1>{{ $item->title }}</h1>
    </div>  
    <div class="container-p ">
        <p>{{ $item->subtitle }}</p>
        <p>{{ $item->description }}</p> 
    </div>
    <div class='content'>
        {!! $item->renderBlocks(false) !!}
    </div>
</div>