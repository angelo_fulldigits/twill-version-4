@extends('twill::layouts.form')

@section('contentFields')
    @formField('medias',[
        'name' => 'guide_image',
        'label' => 'Guide image',
    ])
@stop
