@extends('twill::layouts.settings')

@section('contentFields')
    @formField('input', [
        'label' => 'Font size',
        'name' => 'site_font_size',
        'textLimit' => '80', 
        'type' => 'number'
    ])
     @formField('select', [
        'name' => 'site_categories',
        'label' => 'Categories products',
        'unpack' => true,
        'options' => [
            [
                'value' => 'technology',
                'label' => 'Computers & Smartphones'
            ],
            [
                'value' => 'sports',
                'label' => 'Balls'
            ],
            [
                'value' => 'wearing',
                'label' => 'Shoes & wear'
            ],
            [
                'value' => 'design',
                'label' => 'Design & Architecture'
            ],
            [
                'value' => 'education',
                'label' => 'Books & Mangas'
            ],
            [
                'value' => 'entertainment',
                'label' => 'DvD, DvDRip, Bluray, ...'
            ],
        ]
    ])
@stop