@extends('twill::layouts.settings')

@section('contentFields')
    @formField('input', [
        'label' => 'Site title',
        'name' => 'site_title',
        'textLimit' => '80',
        'translated' => true
    ])
     @formField('medias', [
        'label' => 'Cover',
        'name' => 'site_cover',  
        'note' => 'Minimum image width 1300px',
    ])
@stop