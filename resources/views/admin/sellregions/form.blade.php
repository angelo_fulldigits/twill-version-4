@extends('twill::layouts.form')

@section('contentFields')
    @formField('input', [
        'name' => 'information',
        'label' => 'Information',
        'type' => 'textarea',
        'translated' => true,
        'maxlength' => 100
    ])
    @formField('medias', [
        'name' => 'profile',
        'label' => 'Region picture',
    ])
@stop
