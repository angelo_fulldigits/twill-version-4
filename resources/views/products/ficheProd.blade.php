@extends('layouts.app')

@section('title', 'Home Page')
@section('sidebar')
<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-light bg-light shadow fixed-top">
    <div class="container">
        <a class="navbar-brand" href="#">Twillv4.</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item ">
                    <a class="nav-link" href="{{ url('/') }}">Home  </a>
                </li> 
                <li class="nav-item ">
                    <a class="nav-link"  href="{{ url('/homePage') }}">All products</a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link"  href="{{ url('/bucketsPage') }}">Buckets page</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('/logout') }}">Logout</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
@stop
@section('content')
    <div class="top-left mt-lg-5 links"> 
        <a class="float-left" href="{{ url('/homePage') }}"><-- Back</a>
    </div> 
    <div class="img-mtop2 ">
        <h1 class="text-center title">One Product</h1>
    </div>
    <div class="row">
        
        <div class=" float-center col-sm-12 py-2 mt-5">
            <div class="card h-100">
                <div class="card-body">  
                    <div class=" float-left col-sm-5 py-2">
                        <h1 class="text-center">{{ $instance->title }} </h1>
                        <a href="/storage/uploads/{{  $image->uuid }}"><img class="card-img img-mtop img-zoom" src="/storage/uploads/{{  $image->uuid }}" alt="{{  $image->filename }}" > </a>
                        
                    </div>
                    <div class=" float-right col-sm-7 py-2">
                        <h5 class="card-text"><b>Sous-titre : </b>{{$instance->subtitle}}</h5>
                        <h5 class="card-text"><b>Color : </b></h5> 
                            @foreach($blocks as $block) 
                                    @php 
                                        $content = json_decode($block->content, true); 
                                        
                                    @endphp
                                    @if($block->type == "image_with_text") 
                                    <hr/> 
                                    <h5>My Image with text :</h5> 
                                        <div class="container">
                                            <div class="row">
                                                @foreach($imageWithText as $img)
                                                    @if($img->mediable_id == $block->id)
                                                        <div class="ml-2 col-sm-10 py-2">
                                                            <div class="h-100">
                                                                <a href="/storage/uploads/{{ $img->uuid }}"><img class="img-thumbnail zoom" src="/storage/uploads/{{ $img->uuid }}" alt="{{ $img->filename }}"></a>
                                                                <p class="card-text pl-4"><h5>Image-subtitle: </h5> {{ isset($content['image_subtitle']['en']) ? $content['image_subtitle']['en'] : $content['image_subtitle']['fr']   }} </p>
                                                            </div>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            </div>
                                        </div>
                                        
                                    @elseif($block->type == "my_block")
                                    <hr/>
                                    <h5>My block :</h5>
                                        <ul>
                                            <li><p class="card-text"><b>Battery : </b> {{ $content['battery']['en'] }}</p> </li>
                                            <li><p class="card-text"><b>Size :</b> {{ $content['my_block_size'] }} </p></li>
                                        </ul> 
                                    
                                    @elseif($block->type == "quote")
                                    <hr/>
                                    <h5>My quote :</h5>
                                        <blockquote class="blockquote">
                                            <p class="text-small">{{ $content['quote']['en'] }}</p>
                                        </blockquote>
                                    
                                    @elseif($block->type == "paragraph")
                                    <hr/>
                                    <h5>My paragraph :</h5>
                                        <p class="lead">
                                            <p class="text-small">{!! $content['paragraph']['en'] !!}</p>
                                        </p>

                                    @elseif($block->type == "gallery")
                                    <hr/>
                                    <h5>My gallery :</h5>
                                        <div class="container">
                                            <div class="row">    
                                                @foreach($galleryBlocks as $img)  
                                                <div class="col-sm-4 py-2">
                                                    <div class="card zoom h-100">
                                                        <div class="card-body"> 
                                                                <a href="/storage/uploads/{{$img->uuid}}"><img class="card-img img-zoom" src="/storage/uploads/{{$img->uuid}}" alt="{{$img->filename}}"></a>
                                                        </div>
                                                    </div>
                                                </div> 
                                                @endforeach 
                                            </div>
                                        </div>
                                    @endif 
                            @endforeach 
                        <p class="card-text"><b>Weight :  </b></p>
                        <p class="card-text"><b>Description : </b>{{ $instance->description }}</p>
                    </div> 
                </div>
            </div>
        </div>

        {{-- @foreach($prod as $data)
        <h1 style="text-align:center;"> Fiche produit </h1>
        <img class src="/storage/uploads/{{  $image->uuid}}" alt="{{  $image->filename}}" /> 
            <p><b>Titre : </b>{{ $data->title }}</p>
            <p><b>Sous-titre : </b>{{$data->subtitle}}</p> 
            <
            <p><b>Description : </b>Description : {{$data->description}}</p>
        @endforeach --}}
    </div> 
@stop