<!-- app/views/login.blade.php --> 

@extends('layouts.app')

@section('title', 'Buckets Page')
@section('sidebar')
<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-light bg-light shadow fixed-top">
    <div class="container">
        <a class="navbar-brand" href="#">Twillv4.</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item ">
                    <a class="nav-link" href="{{ url('/') }}">Home  </a>
                </li> 
                <li class="nav-item">
                    <a class="nav-link"  href="{{ url('/homePage') }}">All products</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link"  href="{{ url('/bucketsPage') }}">Buckets page</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('/logout') }}">Logout</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
@stop
 

@section('content')     
    <div class="row position-relative m-toph1">
        
    </div> 
    @foreach($distinctsSectionsBucket as $bucketSection)
        <div class="group">
            <div class="item line"></div>
            <div class="item text">{{ $bucketSection->bucket_key }}</div>
            <div class="item line"></div>
        </div>
        
        <div class="row m-top">
            @foreach($products as $data )  
                @foreach($features as $bucket )  
                    @if($bucket->bucket_key == $bucketSection->bucket_key && $bucket->featured_id == $data->product_id)
                        <div class="col-sm-4 py-2">
                            <div class="card  h-100">
                                <div class="card-body">
                                <h4><u>Titre </u> : {{ $data->title }}</h4>
                                    @foreach($images as $img) 
                                        @if($img->mediable_id == $data->product_id)
                                            <a href="en/products/{{ Str::slug($data->title) }}"><img class="card-img zoom" src="/storage/uploads/{{$img->uuid}}" alt="{{$img->filename}}"></a>
                                        @endif
                                    @endforeach
                                    <p class="card-text"><b>Sous-titre : </b>{{$data->subtitle}}</p>
                                    <p class="card-text" limits="200">Description : {{ str_limit($data->description, 150)}}</p>
                                <a href="en/products/{{ Str::slug($data->title) }}" class="btn btn-outline-info float-right">More about</a>
                                </div>
                            </div>
                        </div> 
                    @endif
                @endforeach 
            @endforeach 
            @foreach($guides as $data )  
                @foreach($featuresGuides as $bucket )  
                    @if($bucket->bucket_key == $bucketSection->bucket_key && $bucket->featured_id == $data->guide_id)
                        <div class="col-sm-4 py-2">
                            <div class="card  h-100">
                                <div class="card-body">
                                <h4><u>Titre </u> : {{ $data->title }}</h4>
                                    @foreach($guidesImages as $img) 
                                        @if($img->mediable_id == $data->guide_id)
                                            <a href="en/products/{{ Str::slug($data->title) }}"><img class="card-img zoom" src="/storage/uploads/{{$img->uuid}}" alt="{{$img->filename}}"></a>
                                        @endif
                                    @endforeach 
                                {{-- <a href="en/products/{{ Str::slug($data->title) }}" class="btn btn-outline-info float-right">More about</a> --}}
                                </div>
                            </div>
                        </div> 
                    @endif
                @endforeach 
            @endforeach 
        </div>
    @endforeach
 @stop