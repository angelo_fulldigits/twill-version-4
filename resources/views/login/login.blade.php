<!-- app/views/login.blade.php --> 
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Connected</title> 
    <!-- Fonts --> 
    
    <!-- Styles --> 
    {{-- <link hrel="{{ URL::asset('css/bootstrap.min.css') }}" type="text/css" rel="stylesheet">   --}}
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    {{-- <script src="{{ URL::asset('js/bootstrap.min.js') }}" ></script>
    <script src="{{ URL::asset('js/jquery-1.11.1.min.js') }}"></script> --}}
    <style>
    @import url(https://fonts.googleapis.com/css?family=Raleway:300,400,600);
         body{
            margin: 0;
            font-size: .9rem;
            font-weight: 400;
            line-height: 1.6;
            color: #212529;
            text-align: left;
            background-color: #f5f8fa;
            
        } 

        .navbar-laravel
        {
            box-shadow: 0 2px 4px rgba(0,0,0,.04);
        }

        .navbar-brand , .nav-link, .my-form, .login-form
        {
            font-family: Raleway, sans-serif;
        }

        .my-form
        {
            padding-top: 1.5rem;
            padding-bottom: 1.5rem;
        }

        .my-form .row
        {
            margin-left: 0;
            margin-right: 0;
        }

        .login-form
        {
            padding-top: 1.8rem;
            padding-bottom: 1.5rem;
        }

        .login-form .row
        {
            margin-left: 0;
            margin-right: 0;
        }
    </style>
</head>
<body>
    <div class="container">
        <main class="login-form">
            <div class="cotainer">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-header">Login Page</div>
                            <div class="card-body">
                                {{ Form::open(array('action' => 'API\UserController@login')) }} 
                                    <div class="form-group row">
                                        <label for="email_address" class="col-md-4 col-form-label text-md-right">E-Mail Address</label>
                                        <div class="col-md-6">
                                            <input type="text" id="email_address" class="form-control" name="email" required autofocus>
                                        </div>
                                    </div>
        
                                    <div class="form-group row">
                                        <label for="password" class="col-md-4 col-form-label text-md-right">Password</label>
                                        <div class="col-md-6">
                                            <input type="password" id="password" class="form-control" name="password" required>
                                        </div>
                                    </div>
        
                                    <div class="form-group row">
                                        <div class="col-md-6 offset-md-4">
                                            <div class="checkbox">
                                                <label>
                                                    {{-- <input type="checkbox" name="remember"> Remember Me --}}
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <p>
                                        {{ $errors->first('email') }}
                                        {{ $errors->first('password') }}
                                    </p>   
                                    <div class="col-md-6 offset-md-4">
                                        <button type="submit" class="btn btn-primary">
                                            Login
                                        </button>
                                        <a href="#" class="btn btn-link">
                                            Forgot Your Password?
                                        </a>
                                    </div> 
                                {{ Form::close() }}
                            </div>
                        </div> 
                    </div>
                </div>
            </div> 
                
        </main>
    </div> 
</body>
</html>