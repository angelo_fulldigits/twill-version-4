<?php

return [ 
    'products' => [
        'title' => 'Products',
        'module' => true,
        
    ],
    'sellregions' => [
        'title' => 'Sell\'s Regions',
        'module' => true
    ],
    'guides' => [
        'title' => 'Guides',
        'module' => true
    ],
    'featured' => [
        'title' => 'Features',
        'route' => 'admin.featured.homepage',
        'primary_navigation' => [
            'homepage' => [
                'title' => 'Features',
                'route' => 'admin.featured.homepage',
            ],
        ],
    ],
    'settings' => [
        'title' => 'Settings',
        'route' => 'admin.settings',
        'params' => ['section' => 'mysettings', 'section' => 'mysecondsettings'],
        'primary_navigation' => [
            'mysettings' => [
                'title' => 'My settings',
                'route' => 'admin.settings',
                'params' => ['section' => 'mysettings'] 
            ],
            'mysecondsettings' => [
                'title' => 'My second settings',
                'route' => 'admin.settings',
                'params' => ['section' => 'mysecondsettings'] 
            ],
        ]
    ],
    'projects' => [
        'can' => 'custom-permission',
        'title' => 'Projects',
        'module' => true,
    ],
];
