<?php
return [
    'block_editor' => [
        'block_single_layout' => 'layouts.block',
        'block_views_path' => 'site.blocks',
        'block_views_mappings' => [],
        'block_preview_render_childs' => true,
        'blocks' => [
            'gallery' => [
                'title' => 'Gallery',
                'icon' => 'image',
                'component' => 'a17-block-gallery',
            ],
            'image_with_text' => [
                'title' => 'Image with text',
                'icon' => 'image-text',
                'component' => 'a17-block-image_with_text',
            ],
            'quote' => [
                'title' => 'Quote',
                'icon' => 'quote',
                'component' => 'a17-block-quote',
            ],
            'paragraph' => [
                'title' => 'Paragraph',
                'icon' => 'text',
                'component' => 'a17-block-paragraph',
            ],
            'accordion' => [
                'title' => 'My personnalized block',
                'icon' => 'image-text',
                'component' => 'a17-block-accordion',
            ],
        ],
        'crops' => [
            'cover' => [
                'default' => [
                    [
                        'name' => 'default',
                        'ratio' => 1 / 1,
                        'minValues' => [
                            'width' => 100,
                            'height' => 100,
                        ],
                    ],
                ],
            ],
            'gallery' => [
                'default' => [
                    [
                        'name' => 'default',
                        'ratio' => 16 / 9,
                        'minValues' => [
                            'width' => 1024,
                            'height' => 768,
                        ],
                    ],
                ],
            ],
            'desktop' => [
                    [
                        'name' => 'desktop',
                        'ratio' => 16 / 9,
                        'minValues' => [
                            'width' => 100,
                            'height' => 100,
                        ],
                    ],
                ],
                'tablet' => [
                    [
                        'name' => 'tablet',
                        'ratio' => 4 / 3,
                        'minValues' => [
                            'width' => 100,
                            'height' => 100,
                        ],
                    ],
                ],
                'mobile' => [
                    [
                        'name' => 'mobile',
                        'ratio' => 1,
                        'minValues' => [
                            'width' => 100,
                            'height' => 100,
                        ],
                    ],
                ],
            'image' => [
                'desktop' => [
                    [
                        'name' => 'desktop',
                        'ratio' => 16 / 9,
                        'minValues' => [
                            'width' => 100,
                            'height' => 100,
                        ],
                    ],
                ],
                'tablet' => [
                    [
                        'name' => 'tablet',
                        'ratio' => 4 / 3,
                        'minValues' => [
                            'width' => 100,
                            'height' => 100,
                        ],
                    ],
                ],
                'mobile' => [
                    [
                        'name' => 'mobile',
                        'ratio' => 1,
                        'minValues' => [
                            'width' => 100,
                            'height' => 100,
                        ],
                    ],
                ],
            ],
        ],
        'repeaters' => [
            'my_block' => [
                'title' => 'Accordion',
                'trigger' => 'Add accordion',
                'component' => 'a17-block-my_block',
                'max' => 10,
            ],
        ],
    ],
    'dashboard' => [
        'modules' => [
            'products' => [ // module name if you added a morph map entry for it, otherwise FQCN of the model (eg. App\Models\Project)
                'name' => 'products', // module name
                'label' => 'Products',
                'count' => true, // show total count with link to index of this module
                'create' => true, // show link in create new dropdown
                'activity' => true, // show activities on this module in actities list
                'draft' => true, // show drafts of this module for current user 
                'search' => true, // show results for this module in global search 
                'search_fields' => ['title', 'subtitle', 'description']
            ],
            // 'guides' => [ // module name if you added a morph map entry for it, otherwise FQCN of the model (eg. App\Models\Project)
            //     'name' => 'guides', // module name
            //     'label' => 'Guides',
            //     'count' => true, // show total count with link to index of this module
            //     'create' => true, // show link in create new dropdown
            //     'activity' => true, // show activities on this module in actities list
            //     'draft' => true, // show drafts of this module for current user 
            //     'search' => true, // show results for this module in global search 
            //     'search_fields' => ['title', 'guide']
            // ],  
        ],
        'analytics' => [
            'enabled' => true,
            'service_account_credentials_json' => storage_path('app/analytics/service-account-credentials.json'),
        ], 
    ],
    'enabled' => [
        'users-management' => true,
        'media-library' => true,
        'file-library' => true,
        'dashboard' => true,
        'search' => true,
        'block-editor' => true,
        'buckets' => true,
        'users-image' => false,
        'users-description' => false,
        'site-link' => false,
        'settings' => true,
        'activitylog' => true,
    ],
    'buckets' => [
        'homepage' => [
            'name' => 'Home',
            'buckets' => [
                'home_primary_feature' => [
                    'name' => 'Buckets\'s guides (primary feature)',
                    'bucketables' => [
                        [
                            'module' => 'guides',
                            'name' => 'Guides',
                            'scopes' => ['published' => true],
                        ],
                        [
                            'module' => 'sellregions',
                            'name' => 'SellRegions',
                            'scopes' => ['published' => true],
                        ],
                    ],
                    'max_items' => 10,
                ],
                'home_secondary_features' => [
                    'name' => 'Buckets\'s Products (secondary features)',
                    'bucketables' => [
                        [
                            'module' => 'products',
                            'name' => 'Products',
                            'scopes' => ['published' => true],
                        ],
                    ],
                    'max_items' => 10,
                ],
                'third_features' => [
                    'name' => 'Just my third feature ',
                    'bucketables' => [
                        [
                            'module' => 'products',
                            'name' => 'Products',
                            'scopes' => ['published' => true],
                        ], 
                        [
                            'module' => 'guides',
                            'name' => 'Guides',
                            'scopes' => ['published' => true],
                        ],
                        [
                            'module' => 'sellregions',
                            'name' => 'SellRegions',
                            'scopes' => ['published' => true],
                        ],
                    ],
                    'max_items' => 20,
                ],
            ],
        ],
    ],
];
