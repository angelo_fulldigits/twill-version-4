<?php

namespace App\Providers;

use Laravel\Passport\Passport;
use A17\Twill\Models\Enums\UserRole;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        
        $this->registerPolicies();

        Passport::routes();
       
       /*  Gate::define('list', function ($user) {
            return in_array($user->role_value, [
                UserRole::CUSTOM1,
                UserRole::CUSTOM2,
                UserRole::ADMIN,
            ]);
        });
        

        Gate::define('edit', function ($user) {
            return in_array($user->role_value, [
                UserRole::CUSTOM3, 
                UserRole::ADMIN,
            ]);
        });

        Gate::define('custom-permission', function ($user) {
            return in_array($user->role_value, [
                UserRole::CUSTOM2,
                UserRole::ADMIN,
            ]);
        }); */
    }
}
