<?php

namespace App\Http\Controllers;

use App\Bucket;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Translations\GuideTranslation;
use App\Models\Translations\ProductTranslation;

class BucketsController extends Controller
{
    /**
     * showPublishedBuckets view pages.buckets
     */
    public function showPublishedBuckets(){
        $distinctsSectionsBucket = Bucket::distinctsFeatures();
        
        //products 
        $features = Bucket::getFeaturesByType('products');
        $featuredIds = Bucket::getProductIdFeatured($features);
        $products = ProductTranslation::whereIn('product_id', $featuredIds)->where('active' , 1)->get();   
        $images = Product::getUUIDMedias( 'products', $featuredIds);

        //guides
        $featuresGuides = Bucket::getFeaturesByType('guides');
        $featuredGuidesIds = Bucket::getProductIdFeatured($featuresGuides);
        $guides = GuideTranslation::whereIn('guide_id', $featuredGuidesIds)->where('active' , 1)->get(); 
        $guidesImages = Product::getUUIDMedias( 'guides', $featuredGuidesIds); 
        return view('pages.buckets', compact('products', 'images', 'distinctsSectionsBucket', 'features', 'featuresGuides', 'guidesImages', 'guides'));
    }
     
}
