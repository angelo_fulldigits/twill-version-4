<?php

namespace App\Http\Controllers\Admin;

use A17\Twill\Http\Controllers\Admin\ModuleController;

class GuideController extends ModuleController
{
    protected $moduleName = 'guides';
}
