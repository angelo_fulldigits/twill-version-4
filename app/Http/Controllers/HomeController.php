<?php

namespace App\Http\Controllers;
 
use App\Models\Translations\ProductTranslation;
use Illuminate\Http\Request;
use A17\Twill\Repositories\SettingRepository;
use App\Http\Controllers\API\RestController as RestController; 

class HomeController extends RestController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($lang = null) 
    {  
        if($lang!=null){
            $site_title = app(SettingRepository::class)->byKey('site_title');
        }else{
            $site_title = app(SettingRepository::class)->byKey('site_title');
        }
        $local = $lang; 
       
        return view('welcome', compact('site_title', 'local'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function login(){
        return view('login.login');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function doLogout(){
        Auth::logout(); // log the user out of our application
        return Redirect::to('loginPage'); // redirect the user to the login screen
    }
}
