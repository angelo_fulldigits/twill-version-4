<?php

namespace App\Http\Controllers;

use DB;
use App\Bucket;
use App\Product;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use A17\Twill\Repositories\SettingRepository;
use App\Models\Translations\ProductTranslation;
use App\Http\Controllers\API\RestController as RestController; 

class ProductController extends RestController
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        $products = ProductTranslation::select();
        $site_title = app(SettingRepository::class)->byKey('site_title');
        dd($site_title);
        return view('products.product', compact('products', 'site_title')); 
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll()
    {  
        $products = ProductTranslation::all();
        return $this->sendResponse($products->toArray(), 'Products retrieve successfully.');
    }

    /**
     * Display a one product.
     *  @param  string  $lang
     *  @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function ficheProd($lang, $slug) {
        $unslug = ucwords(str_replace('-', ' ', $slug));
        $prod = ProductTranslation::where('locale', $lang)->where('title', $unslug)->get();
        $instance = new ProductTranslation();
        $instance = $prod[0];  
        $image =  Product::getUUIDMediasProductById( 'products', $instance['product_id'])[0];  
        $blocks = Product::getBlocksByProductId($instance['product_id']); 
        $galleryBlocks = array();
        $tabId = array();
        $imageWithText = array();
        foreach ($blocks as $key => $block) {
            if($block->type == "gallery"){
                $galleryBlocks = Product::getUUIDMedias('blocks', array($block->id));
            }

            if($block->type == "image_with_text" ){
               array_push($tabId, $block->id);
            }
        }   
        $imageWithText = Product::getUUIDMedias('blocks', $tabId); 
        // $galleryBlocks = Product::getUUIDMedias('blocks', );
        //dd($blocks ,$blocks[3], $blocks[3]->content, json_decode($blocks[3]->content));
        // $test = json_decode($blocks[3]->content, true);
        /* test = $blocks[3]->content;  */ 
 
        return view('products.ficheProd', compact('instance', 'image', 'blocks', 'galleryBlocks', 'imageWithText'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function homePage(){ 
        $publishedProds = Product::getPublishedProds();
        $products = ProductTranslation::whereIn('product_id', $publishedProds->toArray())->where('active' , 1)->get();
        $images = Product::getUUIDMedias( 'products', $publishedProds->toArray());
        
        return view('pages.homePage', compact('products', 'images' ));
    }
 
}
