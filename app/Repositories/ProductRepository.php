<?php

namespace App\Repositories;

use A17\Twill\Repositories\Behaviors\HandleBlocks;
use A17\Twill\Repositories\Behaviors\HandleTranslations;
use A17\Twill\Repositories\Behaviors\HandleSlugs;
use A17\Twill\Repositories\Behaviors\HandleMedias;
use A17\Twill\Repositories\Behaviors\HandleRevisions;
use A17\Twill\Repositories\ModuleRepository;
use App\Models\Product;
use App\Translations\ProductTranslation;

class ProductRepository extends ModuleRepository
{
    use HandleBlocks, HandleTranslations, HandleSlugs, HandleMedias, HandleRevisions;

    public function __construct(Product $model)
    {
        $this->model = $model; 
    }

    public function afterSave($object, $fields){
        $this->updateBrowser($object, $fields, 'sellregions');
        parent::afterSave($object, $fields);
    }
    
    public function getFormFields($object){ 
        $fields = parent::getFormFields($object); 
        $fields['browsers']['sellregions'] = $this->getFormFieldsForBrowser($object, 'sellregions');
        return $fields;
    }
}
