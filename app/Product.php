<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Models\Translations\ProductTranslations;
class Product extends Model
{  
 
    public static function getBlocksByProductTranslation(ProductTranslations $item){
        return this::getBlocksByProductId($item->product_id);
    }
    public static function getBlocksByProductId($product_id){
        $prod = DB::table('blocks')
                    ->where('blockable_id' , $product_id)
                    ->orderBy('position', 'asc')
                    ->get();  
        return $prod;
    }
    public static function getPublishedProds(){
        $prod = Product::select('id')->where('published' , 1)->get();  
        return $prod;
    }
    public static function getUUIDMediasProductById($mediable_type , $product_id ){
        $return = DB::table('UUIDMediasProduct')
                    ->where('mediable_id', $product_id)
                    ->where('mediable_type', $mediable_type)->get();
        return $return;
    }

    public static function getUUIDMedias($mediable_type , array $mediable_id = null){
        $return = null;
        if(!empty($mediable_id)){
            $return = DB::table('UUIDMediasProduct')
                ->whereIn('mediable_id', $mediable_id)
                ->where('mediable_type', $mediable_type)->get();
        }else{
            $return = DB::table('UUIDMediasProduct') 
                ->where('mediable_type', $mediable_type)->get();
        }
        return $return;
    }
}
