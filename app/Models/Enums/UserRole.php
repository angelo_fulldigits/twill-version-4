<?php

namespace A17\Twill\Models\Enums;

use MyCLabs\Enum\Enum;

class UserRole extends Enum 
{
    const CUSTOM1 = 'Custom role 1';
    const CUSTOM2 = 'Custom role 2';
    const CUSTOM3 = 'Custom role 3';
    const ADMIN = 'Admin';
}