<?php

namespace App\Models\Slugs;

use A17\Twill\Models\Model;

class GuideSlug extends Model
{
    protected $table = "guide_slugs";
}
