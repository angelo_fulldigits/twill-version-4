<?php

namespace App\Models\Translations;

use A17\Twill\Models\Model;

class ProductTranslation extends Model
{
    protected $fillable = [
        'title',
        'subtitle', 
        'color',
        'weight',
        'price', 
        'description', 
        'ref_prod_id',
        'active',
        'locale',
    ]; 
  /*  protected $attributes = [
        'price' => 0,
    ];*/
}
