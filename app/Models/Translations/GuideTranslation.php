<?php

namespace App\Models\Translations;

use A17\Twill\Models\Model;

class GuideTranslation extends Model
{
    protected $fillable = [
        'title',
        'guide',
        'active',
        'locale',
    ];
}
