<?php

namespace App\Models;

use A17\Twill\Models\Behaviors\HasTranslation;
use A17\Twill\Models\Behaviors\HasSlug;
use A17\Twill\Models\Behaviors\HasMedias;
use A17\Twill\Models\Model;

class Guide extends Model 
{
    use HasTranslation, HasSlug, HasMedias;

    protected $fillable = [
        'published', 
    ];

    // uncomment and modify this as needed if you use the HasTranslation trait
    public $translatedAttributes = [
        'title',
        'guide',
    ];
    
    // uncomment and modify this as needed if you use the HasSlug trait
    public $slugAttributes = [
        'title', 
    ];

    // add checkbox fields names here (published toggle is itself a checkbox)
    public $checkboxes = [
        'published'
    ];

    // uncomment and modify this as needed if you use the HasMedias trait
    public $mediasParams = [
        'guide_image' => [
            'default' => [
                // [
                //     'name' => 'landscape',
                //     'ratio' => 16 / 9,
                // ],
                [
                    'name' => 'portrait',
                    'ratio' => 3 / 4,
                ],
            ], 
        ],
    ];
}
