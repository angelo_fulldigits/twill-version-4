<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Bucket extends Model
{
    public static function distinctsFeatures(){
        return DB::table('features')
                    ->select('bucket_key')
                    ->distinct()
                    ->get();
    }
    public static function getFeaturesByType($featured_type){
       return Bucket::getFeaturesByTypeAndBucketKey($featured_type, null);
    }
    public static function getFeaturesByTypeAndBucketKey($featured_type, $bucket_key = null){
        if($bucket_key != null){
            return DB::table('features')
            ->select('id', 'bucket_key', 'featured_id', 'featured_type', 'position')
            ->where('featured_type', $featured_type)
            ->where('bucket_key', $bucket_key)
            ->get();
        }else{
            return DB::table('features')
                    ->select('id', 'bucket_key', 'featured_id', 'featured_type', 'position')
                    ->where('featured_type', $featured_type) 
                    ->get();
        }
        
    }
    public static function getProductIdFeatured($features){
        $array_assoc = array(); 
        if(!empty($features)){ 
            foreach ($features as $key => $feature) { 
                array_push($array_assoc, $feature->featured_id); 
            }
        }
        return $array_assoc;
    }
}
