<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSellregionsTables extends Migration
{
    public function up()
    {
        Schema::create('sellregions', function (Blueprint $table) {
            
            // this will create an id, a "published" column, and soft delete and timestamps columns
            createDefaultTableFields($table); 
        });

        // remove this if you're not going to use any translated field, ie. using the HasTranslation trait. If you do use it, create fields you want translatable in this table instead of the main table above. You do not need to create fields in both tables.
        Schema::create('sellregion_translations', function (Blueprint $table) {
            createDefaultTranslationsTableFields($table, 'sellregion');
            // add some translated fields
            $table->string('title', 200)->nullable();
            $table->text('information')->nullable();
        });

        // remove this if you're not going to use slugs, ie. using the HasSlug trait
        Schema::create('sellregion_slugs', function (Blueprint $table) {
            createDefaultSlugsTableFields($table, 'sellregion');
        });

        // remove this if you're not going to use revisions, ie. using the HasRevisions trait
        Schema::create('sellregion_revisions', function (Blueprint $table) {
            createDefaultRevisionsTableFields($table, 'sellregion');
        });
    }

    public function down()
    {
        Schema::dropIfExists('sellregion_revisions');
        Schema::dropIfExists('sellregion_translations');
        Schema::dropIfExists('sellregion_slugs');
        Schema::dropIfExists('sellregions');
    }
}
