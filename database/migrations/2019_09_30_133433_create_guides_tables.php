<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGuidesTables extends Migration
{
    public function up()
    {
        Schema::create('guides', function (Blueprint $table) {
            
            // this will create an id, a "published" column, and soft delete and timestamps columns
            createDefaultTableFields($table);
          
        });

        // remove this if you're not going to use any translated field, ie. using the HasTranslation trait. If you do use it, create fields you want translatable in this table instead of the main table above. You do not need to create fields in both tables.
        Schema::create('guide_translations', function (Blueprint $table) {
            createDefaultTranslationsTableFields($table, 'guide');
            // add some translated fields
            $table->string('title', 200)->nullable();
            $table->text('guide')->nullable();
        });

        // remove this if you're not going to use slugs, ie. using the HasSlug trait
        Schema::create('guide_slugs', function (Blueprint $table) {
            createDefaultSlugsTableFields($table, 'guide');
        });

        // remove this if you're not going to use revisions, ie. using the HasRevisions trait
        Schema::create('guide_revisions', function (Blueprint $table) {
            createDefaultRevisionsTableFields($table, 'guide');
        });
    }

    public function down()
    {
        Schema::dropIfExists('guide_revisions');
        Schema::dropIfExists('guide_translations');
        Schema::dropIfExists('guide_slugs');
        Schema::dropIfExists('guides');
    }
}
